package com.example.advancedmicroservices2.service;

import com.example.advancedmicroservices2.dto.request.AddressRequest;
import com.example.advancedmicroservices2.dto.request.BranchRequest;
import com.example.advancedmicroservices2.dto.response.AddressResponse;

import java.util.List;

public interface AddressService {
    AddressResponse getById(Long addressId);

    List<AddressResponse> getAllAddresses();

    AddressResponse create(Long branchId, AddressRequest request);

    AddressResponse update(Long addressId, AddressRequest request);

    void deleteAddressById(Long addressId);

    void deleteAllAddresses();
}
