package com.example.advancedmicroservices2.service;

import com.example.advancedmicroservices2.dto.request.MarketRequest;
import com.example.advancedmicroservices2.dto.response.MarketResponse;

import java.util.List;

public interface MarketService {
    MarketResponse getById(Long marketId);

    List<MarketResponse> getAll();

    MarketResponse create(MarketRequest request);

    MarketResponse update(Long marketId, MarketRequest request);

    void deleteById(long marketId);

    void deleteAll();
}
