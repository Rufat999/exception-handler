package com.example.advancedmicroservices2.service;

import com.example.advancedmicroservices2.dto.request.AddressRequest;
import com.example.advancedmicroservices2.dto.request.BranchRequest;
import com.example.advancedmicroservices2.dto.response.AddressResponse;
import com.example.advancedmicroservices2.exception.BadRequestException;
import com.example.advancedmicroservices2.exception.ErrorCodes;
import com.example.advancedmicroservices2.exception.NotFoundException;
import com.example.advancedmicroservices2.model.Address;
import com.example.advancedmicroservices2.model.Branch;
import com.example.advancedmicroservices2.repository.AddressRepository;
import com.example.advancedmicroservices2.repository.BranchRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor

public class AddressServiceImpl implements AddressService {

    private final BranchRepository branchRepository;
    private final AddressRepository addressRepository;
    private final ModelMapper modelMapper;


    @Override
    public AddressResponse getById(Long addressId) {
        Address address = addressRepository.findById(addressId)
                .orElseThrow(() -> new NotFoundException(ErrorCodes.ADDRESS_NOT_FOUND));
        AddressResponse addressResponse = modelMapper.map(address, AddressResponse.class);
        return addressResponse;
    }

    @Override
    public List<AddressResponse> getAllAddresses() {
        List<Address> addressList = addressRepository.findAll();
        List<AddressResponse> addressResponseList = addressList.stream().map(address -> modelMapper.map(address, AddressResponse.class)).collect(Collectors.toList());
        return addressResponseList;
    }

    @Override
    public AddressResponse create(Long branchId, AddressRequest request) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new NotFoundException(ErrorCodes.BRANCH_NOT_FOUND));
        Address address1 = branch.getAddress();
        Address address = modelMapper.map(request, Address.class);
        if (address1 != null) {
            if (address1.getName().equals(address.getName())) {
                throw new BadRequestException(ErrorCodes.ADDRESS_ALREADY_EXISTS);
            }
        }
        branch.setAddress(address);
        addressRepository.save(address);
        AddressResponse addressResponse = modelMapper.map(address, AddressResponse.class);
        return addressResponse;
    }

    @Override
    public AddressResponse update(Long addressId, AddressRequest request) {
        Address address = addressRepository.findById(addressId)
                .orElseThrow(() -> new NotFoundException(ErrorCodes.ADDRESS_NOT_FOUND));
        address.setName(request.getName());
        addressRepository.save(address);
        AddressResponse addressResponse = modelMapper.map(address, AddressResponse.class);
        return addressResponse;
    }

    @Override
    public void deleteAddressById(Long addressId) {
        addressRepository.deleteById(addressId);
    }

    @Override
    public void deleteAllAddresses() {
        addressRepository.deleteAll();
    }
}
