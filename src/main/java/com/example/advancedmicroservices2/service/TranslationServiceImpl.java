package com.example.advancedmicroservices2.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
@RequiredArgsConstructor
public class TranslationServiceImpl {

    private final MessageSource messageSource;

    public String findBykey(String key, String lang, Object... arguments){
        try{
            return messageSource.getMessage(key, arguments, new Locale(lang, lang));
        }
        catch (NoSuchMessageException exception) {
            return key;
        }
    }
}
