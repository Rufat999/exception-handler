package com.example.advancedmicroservices2.service.specification;

import com.example.advancedmicroservices2.dto.response.MarketResponse;
import com.example.advancedmicroservices2.model.Market;
import com.example.advancedmicroservices2.repository.MarketRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class MarketSpecification {

    private final ModelMapper modelMapper;
    private final MarketRepository marketRepository;

    public List<MarketResponse> getMarketNameLike(){
        List<Market> market = marketRepository.findAll(nameLike("I"));
        log.info("List is " + market);
        List<MarketResponse> marketResponseList = market.stream().map(market1 -> modelMapper.map(market1, MarketResponse.class)).collect(Collectors.toList());
        log.info("MarketResponseList is " + marketResponseList);
        return marketResponseList;
    }

    private Specification<Market> nameLike(String name){
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(Market.Fields.name), "%" + name + "%");
    }
}
