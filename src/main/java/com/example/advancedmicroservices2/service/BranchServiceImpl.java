package com.example.advancedmicroservices2.service;

import com.example.advancedmicroservices2.dto.request.BranchRequest;
import com.example.advancedmicroservices2.dto.response.BranchResponse;
import com.example.advancedmicroservices2.exception.BadRequestException;
import com.example.advancedmicroservices2.exception.ErrorCodes;
import com.example.advancedmicroservices2.exception.NotFoundException;
import com.example.advancedmicroservices2.model.Branch;
import com.example.advancedmicroservices2.model.Market;
import com.example.advancedmicroservices2.repository.BranchRepository;
import com.example.advancedmicroservices2.repository.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {

    private final MarketRepository marketRepository;
    private final BranchRepository branchRepository;
    private final ModelMapper modelMapper;


    @Override
    public BranchResponse getById(Long branchId) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new BadRequestException(ErrorCodes.BRANCH_NOT_FOUND));

        BranchResponse branchResponse = modelMapper.map(branch, BranchResponse.class);
        return branchResponse;
    }

    @Override
    public List<BranchResponse> getAll() {
        List<Branch> branchList = branchRepository.findAll();

        List<BranchResponse> branchResponseList = branchList.stream().map(branch -> modelMapper.map(branch, BranchResponse.class)).collect(Collectors.toList());
        return branchResponseList;
    }

    @Override
    public BranchResponse create(Long marketId, BranchRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new NotFoundException(ErrorCodes.MARKET_NOT_FOUND));

        Branch branch = modelMapper.map(request, Branch.class);
        Set<Branch> branchList = market.getBranches();
        for (Branch branch1 : branchList) {
            if (branch1.getName().equals(branch.getName())){
                throw new BadRequestException(ErrorCodes.BRANCH_ALREADY_EXISTS);
            }
        }
            market.getBranches().add(branch);
            branch.setMarket(market);
            branchRepository.save(branch);
            BranchResponse branchResponse = modelMapper.map(branch, BranchResponse.class);
            return branchResponse;
    }

    @Override
    public BranchResponse update(Long branchId, BranchRequest request) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new NotFoundException(ErrorCodes.BRANCH_NOT_FOUND));
        branch.setName(request.getName());
        branch.setCountOfEmployee(request.getCountOfEmployee());
        branchRepository.save(branch);
        BranchResponse branchResponse = modelMapper.map(branch, BranchResponse.class);
        return branchResponse;
    }

    @Override
    public void deleteBranchById(Long branchId) {
        branchRepository.deleteById(branchId);
    }

    @Override
    public void deleteAllBranches() {
        branchRepository.deleteAll();
    }
}
