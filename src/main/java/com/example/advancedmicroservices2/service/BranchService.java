package com.example.advancedmicroservices2.service;

import com.example.advancedmicroservices2.dto.request.BranchRequest;
import com.example.advancedmicroservices2.dto.response.BranchResponse;

import java.util.List;

public interface BranchService {
    BranchResponse getById(Long branchId);

    List<BranchResponse> getAll();

    BranchResponse create(Long marketId ,BranchRequest request);

    BranchResponse update(Long branchId, BranchRequest request);

    void deleteBranchById(Long branchId);

    void deleteAllBranches();
}
