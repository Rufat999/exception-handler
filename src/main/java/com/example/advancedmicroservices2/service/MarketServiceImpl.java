package com.example.advancedmicroservices2.service;

import com.example.advancedmicroservices2.dto.request.MarketRequest;
import com.example.advancedmicroservices2.dto.response.MarketResponse;
import com.example.advancedmicroservices2.exception.BadRequestException;
import com.example.advancedmicroservices2.exception.ErrorCodes;
import com.example.advancedmicroservices2.exception.NotFoundException;
import com.example.advancedmicroservices2.model.Market;
import com.example.advancedmicroservices2.repository.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MarketServiceImpl implements MarketService {

    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;

    @Override
    public MarketResponse getById(Long marketId) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new NotFoundException(ErrorCodes.MARKET_NOT_FOUND));
        MarketResponse marketResponse = modelMapper.map(market, MarketResponse.class);
        return marketResponse;
    }

    @Override
    public List<MarketResponse> getAll() {
        List<Market> marketList = marketRepository.findAll();

        List<MarketResponse> marketResponseList = marketList.stream().map(market -> modelMapper.map(market, MarketResponse.class)).collect(Collectors.toList());
        return marketResponseList;
    }

    @Override
    public MarketResponse create(MarketRequest request) {
        Market marketInDB = marketRepository.searchMarketThisName(request.getName());
        if (marketInDB != null){
            throw new BadRequestException(ErrorCodes.MARKET_ALREADY_EXISTS);
        }
        Market market = modelMapper.map(request, Market.class);
        marketRepository.save(market);
        MarketResponse marketResponse = modelMapper.map(market, MarketResponse.class);
        return marketResponse;
    }

    @Override
    public MarketResponse update(Long marketId, MarketRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new NotFoundException(ErrorCodes.MARKET_NOT_FOUND));
        market.setName(request.getName());
        marketRepository.save(market);
        MarketResponse marketResponse = modelMapper.map(market, MarketResponse.class);
        return marketResponse;
    }

    @Override
    public void deleteById(long marketId) {
        marketRepository.deleteById(marketId);
    }

    @Override
    public void deleteAll() {
        marketRepository.deleteAll();
    }
}
