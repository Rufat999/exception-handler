package com.example.advancedmicroservices2.dto.response;

import com.example.advancedmicroservices2.model.Branch;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MarketResponse {

    Long id;

    String name;

    Set<Branch> branches;
}
