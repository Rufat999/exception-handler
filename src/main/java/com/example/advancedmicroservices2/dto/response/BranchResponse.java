package com.example.advancedmicroservices2.dto.response;

import com.example.advancedmicroservices2.model.Address;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BranchResponse {

    Long id;

    String name;
    Integer countOfEmployee;
    Address address;
}
