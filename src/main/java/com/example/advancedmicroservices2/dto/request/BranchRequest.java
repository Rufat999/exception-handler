package com.example.advancedmicroservices2.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BranchRequest {

    @NotBlank
    @Size(min = 2)
    String name;

    @Positive
    Integer countOfEmployee;
}
