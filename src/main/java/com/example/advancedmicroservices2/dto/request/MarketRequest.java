package com.example.advancedmicroservices2.dto.request;

import com.example.advancedmicroservices2.annotation.ColumnNotBlank;
import com.example.advancedmicroservices2.model.Branch;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MarketRequest {

    @Size(min = 2)
    @ColumnNotBlank
    String name;
    Set<Branch> branches;
}
