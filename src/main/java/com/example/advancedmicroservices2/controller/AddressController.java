package com.example.advancedmicroservices2.controller;

import com.example.advancedmicroservices2.dto.request.AddressRequest;
import com.example.advancedmicroservices2.dto.response.AddressResponse;
import com.example.advancedmicroservices2.service.AddressService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/address")
public class AddressController {

    private final AddressService addressService;

    @GetMapping("/{addressId}")
    public AddressResponse getById(@PathVariable Long addressId) {
        return addressService.getById(addressId);
    }

    @GetMapping("/all")
    public List<AddressResponse> getAllAddresses() {
        return addressService.getAllAddresses();
    }

    @PostMapping("/{branchId}/create")
    public AddressResponse create(@PathVariable Long branchId, @RequestBody @Valid AddressRequest request) {
        return addressService.create(branchId, request);
    }

    @PutMapping("/{addressId}/update")
    public AddressResponse update(@PathVariable Long addressId, @RequestBody AddressRequest request) {
        return addressService.update(addressId, request);
    }

    @DeleteMapping("/delete/{addressId}")
    public void deleteAddressById(@PathVariable Long addressId) {
        addressService.deleteAddressById(addressId);
    }

    @DeleteMapping("/delete/all")
    public void deleteAllAddresses() {
        addressService.deleteAllAddresses();
    }
}
