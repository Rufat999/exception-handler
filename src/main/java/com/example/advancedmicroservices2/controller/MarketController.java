package com.example.advancedmicroservices2.controller;

import com.example.advancedmicroservices2.dto.request.MarketRequest;
import com.example.advancedmicroservices2.dto.response.MarketResponse;
import com.example.advancedmicroservices2.service.MarketService;
import com.example.advancedmicroservices2.service.specification.MarketSpecification;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/market")
@Slf4j
public class MarketController {

    private final MarketService marketService;
    private final MarketSpecification marketSpecification;

    @GetMapping("/{marketId}")
    public MarketResponse getById(@PathVariable Long marketId){
        return marketService.getById(marketId);
    }

    @GetMapping("/all")
    public List<MarketResponse> getAll(){
        return marketService.getAll();
    }

    @PostMapping("/create")
    public MarketResponse create(@RequestBody @Valid MarketRequest request){
        return marketService.create(request);
    }

    @PutMapping("/update/{marketId}")
    public MarketResponse update(@PathVariable Long marketId, @RequestBody MarketRequest request){
        return marketService.update(marketId, request);
    }

    @DeleteMapping("/delete/{marketId}")
    public void deleteById(@PathVariable long marketId){
        marketService.deleteById(marketId);
    }

    @DeleteMapping("/delete/all")
    public void deleteAll(){
        marketService.deleteAll();
    }

    @GetMapping("/find/byName/like")
    public List<MarketResponse> getMarketNameLike(){
        return marketSpecification.getMarketNameLike();
    }
}
