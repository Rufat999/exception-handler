package com.example.advancedmicroservices2.controller;

import com.example.advancedmicroservices2.dto.request.BranchRequest;
import com.example.advancedmicroservices2.dto.response.BranchResponse;
import com.example.advancedmicroservices2.service.BranchService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/branch")
public class BranchController {

    private final BranchService branchService;

    @GetMapping("/{branchId}")
    public BranchResponse getById(@PathVariable Long branchId){
        return branchService.getById(branchId);
    }

    @GetMapping("/all")
    public List<BranchResponse> getAll(){
        return branchService.getAll();
    }

    @PostMapping("/{marketId}/create")
    public BranchResponse create(@PathVariable Long marketId, @RequestBody @Valid BranchRequest request){
        return branchService.create(marketId, request);
    }

    @PutMapping("/update/{branchId}")
    public BranchResponse update(@PathVariable Long branchId, @RequestBody BranchRequest request){
        return branchService.update(branchId, request);
    }

    @DeleteMapping("/delete/{branchId}")
    public void deleteBranchById(@PathVariable Long branchId){
        branchService.deleteBranchById(branchId);
    }

    @DeleteMapping("/delete/all")
    public void deleteAllBranches(){
        branchService.deleteAllBranches();
    }
}
