package com.example.advancedmicroservices2.annotation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class ColumnNotBlankValidator implements ConstraintValidator<ColumnNotBlank, String> {
    @Override
    public boolean isValid(String inputData, ConstraintValidatorContext context) {
        return inputData != null && inputData.toString().trim().length() > 0;
    }
}
