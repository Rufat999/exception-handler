package com.example.advancedmicroservices2.repository;

import com.example.advancedmicroservices2.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {

    Address findAddressByName(String name);
}
