package com.example.advancedmicroservices2.repository;

import com.example.advancedmicroservices2.model.Market;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MarketRepository extends JpaRepository<Market, Long>, JpaSpecificationExecutor<Market> {


    //    @Query("select m from Market m join fetch m.branches b join fetch b.address a")
//    @EntityGraph(attributePaths = {"branches", "branches.address"})
    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, value = "market-branch-address")
    List<Market> findAllBy();

    @Query(value = "select m from Market m where m.name = :name")
    Market searchMarketThisName(String name);
}
