package com.example.advancedmicroservices2;

import com.example.advancedmicroservices2.model.Branch;
import com.example.advancedmicroservices2.model.Market;
import com.example.advancedmicroservices2.repository.BranchRepository;
import com.example.advancedmicroservices2.repository.MarketRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class AdvancedMicroservices2Application implements CommandLineRunner {

    private final MarketRepository marketRepository;
    private final BranchRepository branchRepository;

    public static void main(String[] args) {
        SpringApplication.run(AdvancedMicroservices2Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Working Query");
        List<Market> all = marketRepository.findAllBy();
        log.info("QueryWorking");
        for (Market market : all) {
            System.out.println(market);
        }
    }
}
