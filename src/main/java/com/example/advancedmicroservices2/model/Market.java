package com.example.advancedmicroservices2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;

import java.util.Set;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@FieldNameConstants
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
//@NamedQueries({
//        @NamedQuery(name = "findBranchesByMarketId", query = "select b from Branch b where b.market.id = :id")
//})

@NamedEntityGraphs(
        @NamedEntityGraph(
                name = "market-branch-address",
                attributeNodes = {
                        @NamedAttributeNode(value = "branches")
                }
        )
)
public class Market {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;

    @JsonIgnore
    @OneToMany(mappedBy = "market", fetch = FetchType.EAGER)
    Set<Branch> branches;
}
