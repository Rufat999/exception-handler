package com.example.advancedmicroservices2.exception;

import lombok.Getter;

@Getter
public class BadRequestException extends RuntimeException {

    public final ErrorCodes errorCode;

    public BadRequestException(ErrorCodes errorCode){
        this.errorCode = errorCode;
    }
}
