package com.example.advancedmicroservices2.exception;

import lombok.Getter;

@Getter
public class InternalServerError extends RuntimeException{

    private final ErrorCodes errorCode;

    public InternalServerError(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}
