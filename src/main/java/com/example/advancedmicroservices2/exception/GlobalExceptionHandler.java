package com.example.advancedmicroservices2.exception;

import com.example.advancedmicroservices2.dto.response.ErrorResponseDTO;
import com.example.advancedmicroservices2.service.TranslationServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE;

@ControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    private final TranslationServiceImpl translationService;

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorResponseDTO> handleBadRequestException(BadRequestException exception, WebRequest webRequest) {
        var lang = webRequest.getHeader(ACCEPT_LANGUAGE) == null ? "en" : webRequest.getHeader(ACCEPT_LANGUAGE);
        exception.printStackTrace();

        return ResponseEntity.status(400).body(ErrorResponseDTO.builder()
                .status(400)
                .title("Exception")
                .details(translationService.findBykey(exception.getErrorCode().name(), lang))
                .build());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponseDTO> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception, WebRequest webRequest) {
        exception.printStackTrace();

        List<String> errors = exception.getBindingResult().getFieldErrors()
                .stream()
                .map(FieldError::getDefaultMessage)
                .collect(Collectors.toList());

        return ResponseEntity.status(400).body(ErrorResponseDTO.builder()
                .status(400)
                .title("Exception")
                .details(errors.toString())
                .build());
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorResponseDTO> handleNotFoundException(NotFoundException exception, WebRequest webRequest) {
        var lang = webRequest.getHeader(ACCEPT_LANGUAGE) == null ? "en" : webRequest.getHeader(ACCEPT_LANGUAGE);
        exception.printStackTrace();

        return ResponseEntity.status(404).body(ErrorResponseDTO.builder()
                .status(404)
                .title("Exception")
                .details(translationService.findBykey(exception.getErrorCode().name(), lang))
                .build());
    }


    @ExceptionHandler(InternalServerError.class)
    public ResponseEntity<ErrorResponseDTO> handleInternalServerError(InternalServerError exception, WebRequest webRequest) {
        var lang = webRequest.getHeader(ACCEPT_LANGUAGE) == null ? "en" : webRequest.getHeader(ACCEPT_LANGUAGE);
        exception.printStackTrace();

        return ResponseEntity.status(500).body(ErrorResponseDTO.builder()
                .status(500)
                .title("Exception")
                .details(translationService.findBykey(exception.getErrorCode().name(), lang))
                .build());
    }
}
