package com.example.advancedmicroservices2.exception;

public enum ErrorCodes {

    MARKET_NOT_FOUND,
    BRANCH_NOT_FOUND,
    ADDRESS_NOT_FOUND,
    MARKET_ALREADY_EXISTS,
    BRANCH_ALREADY_EXISTS,
    ADDRESS_ALREADY_EXISTS
}
