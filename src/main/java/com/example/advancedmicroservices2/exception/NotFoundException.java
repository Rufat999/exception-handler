package com.example.advancedmicroservices2.exception;

import lombok.Getter;

@Getter
public class NotFoundException extends RuntimeException {

    private final ErrorCodes errorCode;

    public NotFoundException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}
